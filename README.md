# Bee-Done

This is our project for 2020's Download Innovation Festival, it is a progressive web app developed in Flutter, it allows users to create tasks and complete them just like bees.



## Requirements to Compile

##### Client Side

-  Flutter, Channel beta, 1.22.0-12.3.pre
  
  The flutter project is located at [/app](/app) directory

##### Server Side

- MySQL DB

- PHP 7.3

- NodeJS 12.13.1
  
  To run locally the project it's necessary to include a nodeJS instance, a web service with PHP and a DB
  All the server source files, like PHP API, NodeJS Chat, and the DB dump, can be found at the directory [/server](/server)
  

## Demo

##### Web

The web release is hosted and is accessible to perform test at the following URL

[Bee-Done Web](https://hackathon.mattiaeffendi.me/web)

The entire site can be found at the directory [/build/web](/build/web)

##### Mobile

The APK for mobile installation is located at [/build/android/app-release.apk](/build/android/app-release.apk)

Download it: [Click here!](https://gitlab.com/download-event-2020/paleo5id/-/releases/1.0)

## UI Guide

The complete User Interface Guide can be found at [/doc/UI_Guide.md](/doc/UI_Guide.md)

## Test

To test you can use one of the following credential, or you can create a new user by yourself as described in [/doc/UI_Guide.md](/doc/UI_Guide.md)

User: simone@simone.com

Pass: simonesimone

User: enzo@enzo.com

Pass: enzoenzo

User: andrea@andrea.com

Pass: andreaandrea



## WIP Features

- Chat:
  
  Notifications and concurrent processes, fast replies

- Task:
  
  Task research
  
  Hashtags system
  
  More Topics

- User:
  
  Reputation ad ranking system
  
  Coins and incentive system

## Future Features

- Map that shows near tasks

- Charts for reputation

- Animation on creating task
  
  

## Contributing

Baptiste Simone: `baptistesimone19@gmail.com`

Colpani Filippo: `filippocolpani@gmail.com`

Effendi Mattia: `mattia.effendi@gmail.com`

Salvi Niccolò: `nicco.salvi@gmail.com`

Sechi Simone: `simone.sechi24@gmail.com`
