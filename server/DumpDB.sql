-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Oct 01, 2020 at 06:48 AM
-- Server version: 10.2.33-MariaDB
-- PHP Version: 7.3.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `hackathon`
--

-- --------------------------------------------------------

--
-- Table structure for table `hashtags`
--

CREATE TABLE `hashtags` (
  `ID` int(11) NOT NULL,
  `hashtag` varchar(100) NOT NULL,
  `topic` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `hashtags`
--

INSERT INTO `hashtags` (`ID`, `hashtag`, `topic`) VALUES
(1, 'php', 1),
(3, 'javascript', 1),
(4, 'web', 1),
(5, 'frontend', 1),
(6, 'sito', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tasks`
--

CREATE TABLE `tasks` (
  `ID` int(11) NOT NULL,
  `ID_topic` int(11) DEFAULT NULL,
  `data_inizio` int(11) DEFAULT NULL,
  `data_fine` int(11) DEFAULT NULL,
  `durata` int(11) DEFAULT NULL,
  `utente_creatore` int(11) DEFAULT NULL,
  `citta` varchar(100) DEFAULT NULL,
  `utenti_proposti` text DEFAULT NULL,
  `stato` int(11) DEFAULT NULL,
  `titolo` varchar(200) DEFAULT NULL,
  `descrizione` varchar(280) DEFAULT NULL,
  `tipo_task` varchar(15) DEFAULT NULL,
  `hashtag` varchar(300) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tasks`
--

INSERT INTO `tasks` (`ID`, `ID_topic`, `data_inizio`, `data_fine`, `durata`, `utente_creatore`, `citta`, `utenti_proposti`, `stato`, `titolo`, `descrizione`, `tipo_task`, `hashtag`) VALUES
(1, 3, 1601548010, NULL, 3, 1, 'bergamo', NULL, -1, 'Spesa', 'Ho bisogno di latte', '0', '0'),
(2, 6, 1601548071, NULL, 4, 1, 'milano', NULL, -1, 'Help with homework', 'My son is a noob in english, please help me', '0', '0'),
(3, 1, 1601548241, NULL, 128, 2, 'roma', NULL, -1, 'Videogames developer', 'i need a developer for my cooking mama games', '0', '0'),
(4, 10, 1601548425, NULL, 2, 3, 'busto arsizio', NULL, -1, 'i need a banch', 'i\'m looking for a banch near me', '0', '0'),
(5, 10, 1601548633, NULL, 6, 5, 'Milan', NULL, -1, 'tour', 'i need someone that could take me around Milan', '0', '0'),
(6, 5, 1601548699, NULL, 1, 5, 'napoli', NULL, -1, 'League of Legends', 'Hi, i\'m looking for someone for a match in LoL', '0', '0'),
(7, 5, 1601548913, NULL, 1, 6, 'verona', NULL, -1, 'LoL', 'Hi i\'m looking for a jungler in my competitive team', '0', '0'),
(8, 5, 1601549052, NULL, 10, 6, 'milano', NULL, -1, 'Rocket league', 'i\'m doing online lessons but i\'m too powerfull for this normal people so i\'m going to play a match in rocket league.', '0', '0'),
(9, 4, 1601549259, NULL, 10, 7, 'bergamo', NULL, -1, 'I need a chef', 'I need a chef for my new restourant', '0', '0');

-- --------------------------------------------------------

--
-- Table structure for table `topics`
--

CREATE TABLE `topics` (
  `ID` int(11) NOT NULL,
  `nome` varchar(200) NOT NULL,
  `colore` varchar(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `topics`
--

INSERT INTO `topics` (`ID`, `nome`, `colore`) VALUES
(1, 'Programming', 'FF0000'),
(2, 'Home Automation', '0000FF'),
(3, 'House and cleaning', 'FF00FF'),
(4, 'Cooking', '00FF00'),
(5, 'Videogames', 'AA00BA'),
(6, 'Education', '154791'),
(7, 'Handmade', ''),
(8, 'Electronics', ''),
(9, 'Gardening', ''),
(10, 'Do it yourself', '');

-- --------------------------------------------------------

--
-- Table structure for table `utenti`
--

CREATE TABLE `utenti` (
  `ID` int(11) NOT NULL,
  `mail` varchar(200) DEFAULT NULL,
  `password` varchar(500) DEFAULT NULL,
  `nickname` varchar(200) DEFAULT '',
  `coins` int(11) DEFAULT 0,
  `reputazione` int(11) DEFAULT 0,
  `descrizione` varchar(1000) DEFAULT '',
  `nome` varchar(200) DEFAULT NULL,
  `cognome` varchar(200) DEFAULT NULL,
  `topic_preferito` int(11) DEFAULT 0,
  `citta` varchar(100) DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `utenti`
--

INSERT INTO `utenti` (`ID`, `mail`, `password`, `nickname`, `coins`, `reputazione`, `descrizione`, `nome`, `cognome`, `topic_preferito`, `citta`) VALUES
(1, 'simone@simone.com', 'aa93a705e6e1a3b639900ee2b23e6f581832dca1181620b9722fcf14d67bf1cd', 'Simone', 0, 0, '', 'Simone', 'Baptiste', 5, 'milano'),
(2, 'andrea@andrea.com', '9bef8c685319a0a8b632fc70c9e6d550ee1888277e0be85ffbc036eae0238488', 'Andrea', 0, 0, '', 'Andrea', 'papa', 4, 'bergamo'),
(3, 'filippo@filippo.com', 'b4017f3cd9255cae056df84abfb066908e88227407f6293fa149dde487f517ab', 'filippo', 0, 0, '', 'filippo', 'giuggiola', 1, 'busto arsizio'),
(4, 'Mohamed@Mohamed.com', 'ff03b8150a5d64e50470164bfbf2cd0553be9a743e700290a70a11c51e25df6d', 'Mohamed', 0, 0, '', 'Mohamed', 'LaHood', 4, 'Bergamo'),
(5, 'cinzia@cinzia.com', 'c0662522d877030a089bf1f5952c42779d4b4a9db266b11693c8a4cdfc98eb61', 'cinzia', 0, 0, '', 'cinzia', 'testardo', 6, 'napoli'),
(6, 'paola@paola.com', 'efdedbb9c10a9688ca93d3bf65108451a0a09e549c4183f1ae04ecdd0a183c8c', 'paola', 0, 0, '', 'paola', 'ballini', 5, 'verona'),
(7, 'enzo@enzo.com', 'd01e30a4b1e4b08887e81d0a74c36fefbec93dfb98b3aadf174e803e1e0067d9', 'enzo', 0, 0, '', 'enzo', 'miccio', 6, 'bergamo');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `hashtags`
--
ALTER TABLE `hashtags`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `tasks`
--
ALTER TABLE `tasks`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `topics`
--
ALTER TABLE `topics`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `utenti`
--
ALTER TABLE `utenti`
  ADD PRIMARY KEY (`ID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `hashtags`
--
ALTER TABLE `hashtags`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `tasks`
--
ALTER TABLE `tasks`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `topics`
--
ALTER TABLE `topics`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `utenti`
--
ALTER TABLE `utenti`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
