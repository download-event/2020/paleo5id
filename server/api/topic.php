<?php

    require "connection.php";

    if(isset($_GET["id"])){
        $q = $sql->prepare("SELECT * FROM topics WHERE ID = :id");
        $q->execute(array(":id" => $_GET["id"]));
        if($q->rowCount() > 0){
            $res = $q->fetch(PDO::FETCH_ASSOC);
            echo json_encode(["id" => $res["ID"], "nome" => $res["nome"]]);
        }else{
            echo json_encode(["id" => 0, "nome" => "Not found"]);
        }
    }else{
        $q = $sql->prepare("SELECT * FROM topics");
        $q->execute();
        $res = [];
        foreach($q as $row){
            $resu = ["id" => $row["ID"], "nome" => $row["nome"], "colore" => $row["colore"]];
            $res[] = $resu;
        }
        echo json_encode($res);
    }