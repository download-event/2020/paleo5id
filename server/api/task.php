<?php

    require "connection.php";

    if($_GET["action"] == "search"){
        $q = "SELECT * FROM tasks WHERE ";
        $params = [];
        $binds = [];
        if(isset($_GET["citta"]))
        {
            $params[] = "citta LIKE :citta";
            $binds["citta"] = $_GET["citta"];
        }
        if(isset($_GET["topic"])){
            $params[] = "ID_topic = :topic";
            $binds["topic"] = $_GET["topic"];
        }        
        if(isset($_GET["durata"])){
            $params[] = "durata <= :durata";
            $binds["durata"] = $_GET["durata"];
        }
        if(count($params) == 0){
            $q .= "1";
        }else{
            $q .= $params[0];
            for($i = 1; $i < count($params); $i++){
                $q .= " AND ".$params[$i];
            }
        }
        $qu = $sql->prepare($q);
        $qu->execute($binds);
        $results = [];
        foreach($qu as $row){
            $result = ["id" => $row["ID"], "ID_topic" => $row["ID_topic"], "data_inizio" => $row["data_inizio"], "data_fine" => $row["data_fine"], 
            "durata" => $row["durata"], "utente_creatore" => $row["utente_creatore"], "citta" => $row["citta"], "utenti_proposti" => $row["utenti_proposti"],
            "stato" => $row["stato"], "titolo" => $row["titolo"], "descrizione" => $row["descrizione"], "tipo_task" => $row["tipo_task"], "hashtag" => $row["hashtag"]];
            $results[] = $result;
        }
        $output = ["count" => count($results), "results" => $results];
        echo json_encode($output);
    }elseif($_GET["action"] == "insert"){
        if(isset($_GET["id"]) && isset($_GET["durata"]) && isset($_GET["utente_creatore"]) && isset($_GET["citta"]) && isset($_GET["titolo"]) && isset($_GET["tipo"]) && isset($_GET["hashtag"]))
            $extra = "";
            $extra2 = "";
            $params = [":idt" => $_GET["id"], ":inizio" => time(), ":durata" => $_GET["durata"], ":creatore" => $_GET["utente_creatore"], ":citta" => $_GET["citta"], ":stato" => -1, ":titolo" => $_GET["titolo"], ":tipo" => $_GET["tipo"], ":hashtag" => $_GET["hashtag"]];
            if(isset($_GET["descrizione"])){
                $extra .= ", descrizione";
                $extra2 .= ", :desc";       
                $params[":desc"] = $_GET["descrizione"];         
            }
            $q = "INSERT INTO tasks(ID_topic, data_inizio, durata, utente_creatore, citta, stato, titolo, tipo_task, hashtag$extra) VALUES(:idt, :inizio, :durata, :creatore, :citta, :stato, :titolo, :tipo, :hashtag$extra2)";

            $qq = $sql->prepare($q);
            $qq->execute($params);
        }else{
            echo json_encode(["ok" => false, "message" => "Invalid parameters."]);
        }