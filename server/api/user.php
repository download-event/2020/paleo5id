<?php

    require "connection.php";

    if(isset($_GET["id"])){
        $q = $sql->prepare("SELECT * FROM utenti WHERE ID = :id");
        $q->execute(array(":id" => $_GET["id"]));
        if($q->rowCount() > 0){
            $fetch = $q->fetch(PDO::FETCH_ASSOC);
            $res = ["ok" => true, "user_data" => ["id" => $fetch["ID"], "mail" => $fetch["mail"], "nickname" => $fetch["nickname"], "coins" => $fetch["coins"], 
            "reputazione" => $fetch["reputazione"], "descrizione" => $fetch["descrizione"], "nome" => $fetch["nome"], "cognome" => $fetch["cognome"],
            "topic_preferito" => $fetch["topic_preferito"], "citta" => $fetch["citta"]]];
            echo json_encode($res);
        }
    }