<?php

require "connection.php";

if(isset($_GET["nome"]) && isset($_GET["cognome"]) && isset($_GET["mail"]) && isset($_GET["password"]) && isset($_GET["token"])){
    $mail = $_GET["mail"];
    $nome = $_GET["nome"];
    $token = sha1($mail.$nome);

    if($token == $_GET["token"]){
        $q = $sql->prepare('INSERT INTO utenti(nome, cognome, mail, password, nickname, citta, topic_preferito) VALUES(:nom, :cog, :mail, :psw, :nn, :citta, :topi)');
        $q->execute(array(':nom' => $_GET["nome"], ':cog' => $_GET["cognome"], ':mail' => $_GET["mail"], ':psw' => $_GET['password'], ":nn" => $_GET["nickname"], ":citta" => $_GET["citta"], ":topi" => $_GET["topic_preferito"]));
        $res = ["ok" => true, "msg" => "Successfully registered."];
        echo json_encode($res);
    }else{
        $res = ["ok" => false, "msg" => "The token is invalid!"];
        echo json_encode($res);        
    }
}else{
    $res = ["ok" => false, "msg" => "Invalid request: missing one (or more) of the required parameters (nome, cognome, mail, password, token)."];
    echo json_encode($res);       
}