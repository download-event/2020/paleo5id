<?php

require "connection.php";

if(isset($_GET["mail"]) && isset($_GET["password"]) && isset($_GET["token"])){
    $mail = $_GET["mail"];
    $password = $_GET["password"];
    $token = sha1($mail.$password);

    if($token == $_GET["token"]){
        $q = $sql->prepare('SELECT * FROM utenti WHERE mail = :mail AND password = :psw');
        $q->execute(array(':mail' => $mail, ':psw' => $password));
        if($q->rowCount() > 0){  
            $fetch = $q->fetch(PDO::FETCH_ASSOC);
            $res = ["ok" => true, "user_data" => ["id" => $fetch["ID"], "mail" => $fetch["mail"], "nickname" => $fetch["nickname"], "coins" => $fetch["coins"], 
            "reputazione" => $fetch["reputazione"], "descrizione" => $fetch["descrizione"], "nome" => $fetch["nome"], "cognome" => $fetch["cognome"],
            "topic_preferito" => $fetch["topic_preferito"], "citta" => $fetch["citta"]]];
            echo json_encode($res);
        }else{
            $res = ["ok" => false, "msg" => "Login not successful."];
            echo json_encode($res);           
        }

    }else{
        $res = ["ok" => false, "msg" => "The token is invalid!"];
        echo json_encode($res);        
    }
}else{
    $res = ["ok" => false, "msg" => "Invalid request: missing one (or more) of the required parameters (mail, password, token)."];
    echo json_encode($res);       
}