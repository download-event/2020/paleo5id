import 'package:flutter/material.dart';

class Searchbox extends StatefulWidget {
  @override
  _SearchboxState createState() => _SearchboxState();
}

class _SearchboxState extends State<Searchbox> {
  bool activeSearch = false;
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
    
      padding: EdgeInsets.fromLTRB(0, 20, 30, 10),
      height: 80,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Expanded(
            child: TextFormField(
              onFieldSubmitted: (str){
                print(str);
              },
              textInputAction: TextInputAction.go,
              decoration: new InputDecoration(
                suffixIcon: IconButton(
              icon: Icon(Icons.search), onPressed: () {}),
                labelText: "Search for task",
                fillColor: Colors.white,
                border: new OutlineInputBorder(
                  borderRadius: new BorderRadius.circular(20.0),
                  borderSide: new BorderSide(),
                ),
              ),
              validator: (value) {
                if (value.isEmpty) {
                  return 'Please enter a valid mail';
                }

                return null;
              },
            ),
          ),
          
        ],
      ),
    );
  }
}
