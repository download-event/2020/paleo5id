import 'package:flutter/material.dart';
import 'package:hackathonApp/model/Task.dart';
import 'package:hackathonApp/pages/ShowTaskPage.dart';
import 'package:hackathonApp/util/DataProvider.dart';

class TaskViewer extends StatefulWidget {
  final List<Task> tasks;
  TaskViewer(this.tasks);
  @override
  _TaskViewerState createState() => _TaskViewerState();
}

class _TaskViewerState extends State<TaskViewer> {
  List<Widget> tasks;
  @override
  Widget build(BuildContext context) {
    tasks = _buildCards();
    return Container(
        margin: EdgeInsets.symmetric(vertical: 20.0),
        height: 250.0,
        child: ListView(
          scrollDirection: Axis.horizontal,
          children: tasks,
        ));
  }

  List<Widget> _buildCards() {
    List<Widget> w = new List<Widget>();
    widget.tasks.forEach((t) {
      w.add(Container(
        child: Card(
          elevation: 30,
          shadowColor: Colors.black,
          child: InkWell(
            splashColor: Colors.blue.withAlpha(30),
            onTap: () {
               Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => ShowTaskPage(t)),
              );
            },
            child: Container(
                padding: EdgeInsets.all(10),
                width: 300,
                height: 300,
                child: (Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Text(t.titolo,
                        style: TextStyle(color: Colors.amber, fontSize: 18)),
                    SizedBox(height: 20),
                    Text(
                      t.contenuto,
                      style: TextStyle(fontSize: 16),
                      textAlign: TextAlign.center,
                    ),
                    SizedBox(height: 10),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        Row(
                          children: [Icon(Icons.location_city), Text(t.citta)],
                        ),
                        Row(
                          children: [
                            Icon(Icons.watch),
                            Text(t.durata.toString() + " h")
                          ],
                        )
                      ],
                    ),
                    SizedBox(height: 10),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        Row(
                          children: [
                            Icon(Icons.topic),
                            Text(DataProvider.getTopicName(t.idtopic))
                          ],
                        ),
                        Row(
                          children: [
                            Icon(Icons.person),
                            Text(t.creatore.nickname)
                          ],
                        )
                      ],
                    )
                  ],
                ))),
          ),
        ),
      ));
      w.add(SizedBox(width: 10));
    });
    return w;
  }
}
