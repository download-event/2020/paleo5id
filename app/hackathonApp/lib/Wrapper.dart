import 'package:flutter/material.dart';
import 'package:hackathonApp/auth/Login.dart';
import 'package:hackathonApp/auth/SignUp.dart';

class Wrapper extends StatefulWidget {
  @override
  _WrapperState createState() => _WrapperState();
}

class _WrapperState extends State<Wrapper> {
  bool login=true;
  @override
  Widget build(BuildContext context) {
    return login?Login((){setState(() {
      login=false;
    });}):SignUp((){setState(() {
      login=true;
    });});
  }
}
