import 'package:flutter/material.dart';
import 'package:hackathonApp/Wrapper.dart';
import 'package:hackathonApp/chat/AllChatsPage.dart';
import 'package:hackathonApp/chat/ChatModel.dart';
import 'package:hackathonApp/model/User.dart';
import 'package:hackathonApp/pages/Homepage.dart';
import 'package:hackathonApp/pages/NewtaskPage.dart';
import 'package:hackathonApp/pages/SettingsPage.dart';
import 'package:hackathonApp/pages/UserPage.dart';
import 'package:splashscreen/splashscreen.dart';
import 'package:scoped_model/scoped_model.dart';

User user;
void main() {
  runApp(new MaterialApp(
    home: new MyApp(),
  ));
}
class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => new _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  Widget build(BuildContext context) {
    return new SplashScreen(
        seconds: 2,
        navigateAfterSeconds: new AfterSplash(),
        image: new Image.asset("assets/bee.png"),
        backgroundColor: Colors.amber,
        styleTextUnderTheLoader: new TextStyle(),
        photoSize: 100.0,
        onClick: () => print("Flutter Egypt"),
        loaderColor: Colors.black);
  }
}


class AfterSplash extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ScopedModel(
      model: ChatModel(),
      child:MaterialApp(
        debugShowCheckedModeBanner: false,
      title: 'Bee-Done',
      theme: ThemeData(
        brightness: Brightness.dark,
        primaryColor: Colors.amber[800],
        accentColor: Colors.amber,
        cursorColor: Colors.amber,
        primaryColorLight: Colors.amber
        
      ),
      routes: {
        '/home': (context) => HomePage(),
        '/user': (context) => UserPage(),
        '/settings': (context) => SettingsPage(),
        '/addtask':(context) => NewTaskPage(),
        '/chat':(context) => AllChatsPage(null)
      },
      home: Wrapper(),
    ));
  }
}
