import 'package:flutter/material.dart';
import 'package:hackathonApp/auth/Login.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SettingsPage extends StatefulWidget {

  @override
  _SettingsPageState createState() => _SettingsPageState();
}

class _SettingsPageState extends State<SettingsPage> {
  Future<SharedPreferences> _prefs = SharedPreferences.getInstance();
  bool logout=false;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      bottomNavigationBar: BottomAppBar(
        notchMargin: 8,
        shape: CircularNotchedRectangle(),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
              
              IconButton(
                  iconSize: 30,
                  icon: Icon(
                    Icons.home,
                  ),
                  onPressed: () {
                    Navigator.pushNamed(context, '/home');
                  }),
              
              IconButton(
                  iconSize: 30,
                  icon: Icon(Icons.chat_bubble_outline),
                  onPressed: () {
                    Navigator.pushNamed(context, '/chat');
                  }),
                  SizedBox(width: 50),
              IconButton(
                  iconSize: 30,
                  icon: Icon(Icons.settings),
                  color: Colors.amber,
                  onPressed: () {
                    Navigator.pushNamed(context, '/settings');
                  }),
              IconButton(
                  iconSize: 30,
                  icon: Icon(Icons.person),
                  onPressed: () {
                    Navigator.pushNamed(context, '/user');
                  }),
            ],
        ),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      floatingActionButton: FloatingActionButton(
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Image.asset("assets/bee.png"),
        ),
        onPressed: () {Navigator.pushNamed(context, '/addtask');},
      ),
      body: Container(
        padding: EdgeInsets.all(8),
        child: Column(children: [
          SizedBox(height:40),
          Text("Setting",style: TextStyle(fontSize: 24,),),
          SizedBox(height:30),
          RaisedButton(
            color: Colors.red,
            child: Text("Log Out"),
            onPressed: (){  
              setState(() {
                _prefs.then((pref) {
                pref.remove("mail");
                pref.remove("pass");
                Login.logout();
              });
              });
              
            },
          
          )
        ],),
      ),
    );
  }
}