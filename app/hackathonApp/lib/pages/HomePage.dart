import 'package:flutter/material.dart';
import 'package:hackathonApp/main.dart';
import 'package:hackathonApp/model/Task.dart';
import 'package:hackathonApp/util/DataProvider.dart';
import 'package:hackathonApp/widgets/SearchBox.dart';
import 'package:hackathonApp/widgets/TaskViewer.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

class HomePage extends StatefulWidget {
  HomePage();
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  List<Task> taskPerTopic;
  List<Task> taskPerCitta;
  int i=0;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    _refreshData();
    return Scaffold(
        bottomNavigationBar: BottomAppBar(
          notchMargin: 8,
          shape: CircularNotchedRectangle(),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              IconButton(
                  iconSize: 30,
                  icon: Icon(
                    Icons.home,
                    color: Colors.amber,
                  ),
                  onPressed: () {
                    Navigator.pushNamed(context, '/home');
                  }),
              IconButton(
                  iconSize: 30,
                  icon: Icon(Icons.chat_bubble_outline),
                  onPressed: () {
                    Navigator.pushNamed(context, '/chat');
                  }),
              SizedBox(width: 50),
              IconButton(
                  iconSize: 30,
                  icon: Icon(Icons.settings),
                  onPressed: () {
                    Navigator.pushNamed(context, '/settings');
                  }),
              IconButton(
                  iconSize: 30,
                  icon: Icon(Icons.person),
                  onPressed: () {
                    Navigator.pushNamed(context, '/user');
                  }),
            ],
          ),
        ),
        floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
        floatingActionButton: FloatingActionButton(
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Image.asset("assets/bee.png"),
          ),
          onPressed: () {
            Navigator.pushNamed(context, '/addtask');
          },
        ),
        body: RefreshIndicator(
          displacement: 100,
              onRefresh: _refreshData,
                  child: new GestureDetector(
              onTap: () {
                FocusScope.of(context).requestFocus(new FocusNode());
              },
              child: SingleChildScrollView(
                  child: Container(
                padding: EdgeInsets.all(8),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SizedBox(height: 10),
                    Searchbox(),
                    SizedBox(height: 10),
                    Text(
                      "Near you →",
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 18,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    taskPerCitta != null
                        ? taskPerCitta.length > 0
                            ? TaskViewer(taskPerCitta)
                            : Padding(
                                padding: const EdgeInsets.all(16.0),
                                child: Text("Nessun risultato trovato",
                                    style: TextStyle(color: Colors.grey)),
                              )
                        : SpinKitRing(
                            color: Colors.amber,
                            size: 50.0,
                          ),
                    Text(
                      "Your focus Topic →",
                      style: TextStyle(
                          color: Colors.white,
                          fontSize: 18,
                          fontWeight: FontWeight.bold),
                    ),
                    taskPerTopic != null
                        ? taskPerTopic.length > 0
                            ? TaskViewer(taskPerTopic)
                            : Padding(
                                padding: const EdgeInsets.all(16.0),
                                child: Text("Nessun risultato trovato",
                                    style: TextStyle(color: Colors.grey)),
                              )
                        : SpinKitRing(
                            color: Colors.amber,
                            size: 50.0,
                          ),
                  ],
                ),
              ))),
        ));
  }

  Future<void>_refreshData() async {
    await Future.delayed((Duration(seconds: 2)));
    DataProvider.getTasks(null, user.citta, null).then((value) {
      setState(() {
        taskPerCitta = value;
      
      });
    });
    DataProvider.getTasks(user.topicPreferito.toString(), null, null).then((value) {
      setState(() {
        taskPerTopic = value;
      });
    });

    
  }
}
