import 'package:flutter/material.dart';
import 'package:hackathonApp/chat/AllChatsPage.dart';
import 'package:hackathonApp/chat/UserChat.dart';
import 'package:hackathonApp/model/Task.dart';
import 'package:hackathonApp/util/DataProvider.dart';

class ShowTaskPage extends StatefulWidget {
  final Task task;
  ShowTaskPage(this.task);
  @override
  _ShowTaskPageState createState() => _ShowTaskPageState();
}

class _ShowTaskPageState extends State<ShowTaskPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.amber,
          title: Text("Task: " + widget.task.titolo),
        ),
        body: new GestureDetector(
            onTap: () {
              FocusScope.of(context).requestFocus(new FocusNode());
            },
            child: SingleChildScrollView(
                child: Container(
                    padding: EdgeInsets.all(20),
                    child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            widget.task.titolo,
                            style: TextStyle(
                                color: Colors.amber,
                                fontSize: 24,
                                fontWeight: FontWeight.bold),
                          ),
                          SizedBox(height: 20),
                          Text(widget.task.contenuto,
                              style: TextStyle(fontSize: 18)),
                          SizedBox(height: 20),
                          Row(
                            children: [
                              Icon(Icons.topic),
                              SizedBox(
                                width: 10,
                              ),
                              Text(
                                "Topic:\n" +
                                    DataProvider.getTopicName(
                                        widget.task.idtopic),
                                style: TextStyle(fontSize: 18),
                              ),
                            ],
                          ),
                          SizedBox(height: 20),
                          Row(
                            children: [
                              Icon(Icons.watch),
                              SizedBox(
                                width: 10,
                              ),
                              Text(
                                  "Time (hours):\n" +
                                      widget.task.durata.toString(),
                                  style: TextStyle(fontSize: 18)),
                            ],
                          ),
                          SizedBox(height: 20),
                          Row(
                            children: [
                              Icon(Icons.location_city),
                              SizedBox(
                                width: 10,
                              ),
                              Text("City:\n" + widget.task.citta,
                                  style: TextStyle(fontSize: 18)),
                            ],
                          ),
                          SizedBox(
                            height: 20,
                          ),
                          Row(
                            children: [
                              Icon(Icons.person),
                              SizedBox(
                                width: 10,
                              ),
                              Text(
                                  "Task Creator:\n" +
                                      widget.task.creatore.firstname +
                                      " " +
                                      widget.task.creatore.lastname,
                                  style: TextStyle(fontSize: 18)),
                            ],
                          ),
                          SizedBox(height: 20),
                          Center(
                            child: FlatButton(
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(18.0),
                                  side: BorderSide(
                                      color: Colors.amber, width: 2)),
                              color: Color.fromARGB(0, 0, 0, 0),
                              onPressed: () {
                                Navigator.push(
                                  context,
                                  MaterialPageRoute(builder: (context) => AllChatsPage(UserChat(widget.task.creatore.nickname,widget.task.creatore.id.toString()))));
                              },
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.center,
                                children: [
                                  Icon(
                                    Icons.person,
                                    color: Colors.amber,
                                    size: 50,
                                  ),
                                  Text('CONTACT TASK CREATOR',
                                      style: TextStyle(
                                          color: Colors.amber, fontSize: 18)),
                                ],
                              ),
                            ),
                          ),
                        ])))));
  }
}
