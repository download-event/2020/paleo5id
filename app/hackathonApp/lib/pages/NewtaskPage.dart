import 'package:flutter/material.dart';
import 'package:hackathonApp/main.dart';
import 'package:hackathonApp/model/Topic.dart';
import 'package:hackathonApp/util/DataProvider.dart';

class NewTaskPage extends StatefulWidget {
  @override
  _NewTaskPageState createState() => _NewTaskPageState();
}

class _NewTaskPageState extends State<NewTaskPage> {
  final _formKey = GlobalKey<FormState>();

  String title, content,city;
  int topicId, durata;
  Topic dropdownValue;
  @override
  Widget build(BuildContext context) {
    print(DataProvider.topics.length);

    return Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.amber,
          title: Text("Create Task"),
        ),
        body: new GestureDetector(
          onTap: () {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          child: SingleChildScrollView(
            child: Container(
              padding: EdgeInsets.all(20),
              child: Center(
                child: Column(
                  children: [
                    Form(
                      key: _formKey,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          SizedBox(height: 20),
                          Row(
                            children: <Widget>[
                              Expanded(
                                flex: 7,
                                child: TextFormField(
                                  decoration: new InputDecoration(
                                    labelText: "Title",
                                    fillColor: Colors.white,
                                    border: new OutlineInputBorder(
                                      borderRadius:
                                          new BorderRadius.circular(20.0),
                                      borderSide: new BorderSide(),
                                    ),
                                  ),
                                  validator: (value) {
                                    if (value.isEmpty) {
                                      return 'Please enter some text';
                                    }
                                    title = value;
                                    return null;
                                  },
                                ),
                              ),
                              SizedBox(width: 10),
                              Expanded(
                                flex: 3,
                                child: TextFormField(
                                  keyboardType: TextInputType.number,
                                  decoration: new InputDecoration(
                                    labelText: "Max Time (h)",
                                    fillColor: Colors.white,
                                    border: new OutlineInputBorder(
                                      borderRadius:
                                          new BorderRadius.circular(20.0),
                                      borderSide: new BorderSide(),
                                    ),
                                  ),
                                  validator: (value) {
                                    if (value.isEmpty ||
                                        (int.tryParse(value) == null)) {
                                      return 'Please a insert valid Time';
                                    }
                                    durata = int.parse(value);
                                    if (durata <= 0) {
                                      return 'Please a insert valid Time';
                                    }
                                    return null;
                                  },
                                ),
                              ),
                            ],
                          ),
                          SizedBox(height: 20),
                          Container(
                            child: TextFormField(
                              maxLines: null,
                              keyboardType: TextInputType.multiline,
                              decoration: new InputDecoration(
                                labelText: "Description",
                                fillColor: Colors.white,
                                border: new OutlineInputBorder(
                                  borderRadius: new BorderRadius.circular(20.0),
                                  borderSide: new BorderSide(),
                                ),
                              ),
                              validator: (value) {
                                if (value.isEmpty) {
                                  return 'Please enter some text';
                                }
                                content = value;
                                return null;
                              },
                            ),
                          ),
                          SizedBox(height: 20),
                          TextFormField(
                                initialValue: user.citta,
                                  decoration: new InputDecoration(
                                    labelText: "City",
                                    fillColor: Colors.white,
                                    border: new OutlineInputBorder(
                                      borderRadius:
                                          new BorderRadius.circular(20.0),
                                      borderSide: new BorderSide(),
                                    ),
                                  ),
                                  validator: (value) {
                                    if (value.isEmpty) {
                                      return 'Please enter some text';
                                    }
                                    city = value;
                                    return null;
                                  },
                                ),
                          SizedBox(height:20),
                          Center(
                            child: DropdownButton<Topic>(
                              hint: (Text("Choose a Topic")),
                              value: dropdownValue,
                              icon: Icon(Icons.arrow_drop_down),
                              iconSize: 24,
                              elevation: 16,
                              style: TextStyle(color: Colors.deepPurple),
                              underline: Container(
                                height: 2,
                              ),
                              onChanged: (Topic newValue) {
                                setState(() {
                                  dropdownValue = newValue;
                                });
                              },
                              items: DataProvider.topics
                                  .map<DropdownMenuItem<Topic>>((Topic c) {
                                return DropdownMenuItem<Topic>(
                                  value: c,
                                  child: Text(c.nome,
                                      style: TextStyle(
                                          color: Colors.amber, fontSize: 18)),
                                );
                              }).toList(),
                            ),
                          ),
                          SizedBox(height: 20),
                          Center(
                            child: FlatButton(
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(18.0),
                                  side: BorderSide(
                                      color: Colors.amber, width: 2)),
                              color: Color.fromARGB(0, 0, 0, 0),
                              onPressed: () async {
                                if (_formKey.currentState.validate()) {
                                  print("adding task--");
                                  int status = await DataProvider.addTask(
                                      durata,
                                      user.id,
                                      city,
                                      title,
                                      content,
                                      dropdownValue.id);
                                  if (status == 200) {
                                    Navigator.pop(context);
                                  }
                                }
                              },
                              child: Text('CREATE TASK',
                                  style: TextStyle(
                                      color: Colors.amber, fontSize: 18)),
                            ),
                          ),
                          
                        ],
                      ),
                    )
                  ],
                ),
              ),
            ),
          ),
        ));
  }
}
