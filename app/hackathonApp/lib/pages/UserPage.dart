import 'package:flutter/material.dart';
import 'package:hackathonApp/main.dart';
import 'package:hackathonApp/util/DataProvider.dart';

class UserPage extends StatefulWidget {
  @override
  _UserPageState createState() => _UserPageState();
}

class _UserPageState extends State<UserPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      bottomNavigationBar: BottomAppBar(
        notchMargin: 8,
        shape: CircularNotchedRectangle(),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            IconButton(
                iconSize: 30,
                icon: Icon(
                  Icons.home,
                ),
                onPressed: () {
                  Navigator.pushNamed(context, '/home');
                }),
            IconButton(
                iconSize: 30,
                icon: Icon(Icons.chat_bubble_outline),
                onPressed: () {
                    Navigator.pushNamed(context, '/chat');
                  }),
            SizedBox(width: 50),
            IconButton(
                iconSize: 30,
                icon: Icon(Icons.settings),
                onPressed: () {
                  Navigator.pushNamed(context, '/settings');
                }),
            IconButton(
                iconSize: 30,
                icon: Icon(Icons.person),
                color: Colors.amber,
                onPressed: () {
                  Navigator.pushNamed(context, '/user');
                }),
          ],
        ),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      floatingActionButton: FloatingActionButton(
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Image.asset("assets/bee.png"),
        ),
        onPressed: () {Navigator.pushNamed(context, '/addtask');},
      ),
      body: SingleChildScrollView(
        child: Container(
          padding: EdgeInsets.all(8),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(height: 30),
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Container(
                      
                      height: 100,
                      child: Icon(Icons.person,size: 100, color: Colors.amber[200])

                  ),
                  SizedBox(width: 50,),
                  Container(
                    
                    child: Text(
                      "Honey Coins: " + user.coins.toString(),
                      style: TextStyle(fontSize: 18,color: Colors.amber[200]),
                    ),
                  ),
                  
                ],
                
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text(user.firstname +" "+ user.lastname,style:TextStyle(fontSize: 18)),
              ),
              SizedBox(height: 20,),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text("City",style:TextStyle(fontSize: 18)),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Row(children: [
                  Icon(Icons.location_city),
                  SizedBox(width: 10,),
                  Text(user.citta.length>0?user.citta:"Not Set")
                ],),
              ),
              SizedBox(height: 10,),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text("Favourite topic",style:TextStyle(fontSize: 18)),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Row(children: [
                  Icon(Icons.topic),
                  SizedBox(width: 10,),
                  Text(user.citta.length>0?DataProvider.getTopicName(user.topicPreferito):"Not Set")
                ],),
              ),
              SizedBox(height: 10,),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text("Reputation",style:TextStyle(fontSize: 18)),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Row(children: [
                  Image.asset("assets/chart-logo.png",height: 150,),
                  Text(user.reputazione.toDouble().toString(),style: TextStyle(fontSize: 50,color: Colors.green))
                ],),
              )
            ],
          ),
        ),
      ),
    );
  }
}
