import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:hackathonApp/main.dart';
import 'package:hackathonApp/model/User.dart';
import 'package:hackathonApp/pages/HomePage.dart';
import 'package:hackathonApp/util/DataProvider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:crypto/crypto.dart';


class Login extends StatefulWidget {
  Login(this.signin);
  
  final Function signin;
  @override
  _LoginState createState() => _LoginState();

  static void logout() {
    _LoginState.logOut();
  }
  
}
 

class _LoginState extends State<Login> {
  Future<SharedPreferences> _prefs = SharedPreferences.getInstance();

  final _formKey = GlobalKey<FormState>();
  String email = "", password = "";
 
  static bool logged=false;
  static void logOut(){
    logged=false;
    
  }
  
  @override
  // ignore: must_call_super
  void initState() {
      DataProvider.getTopics();
  }

  @override
  Widget build(BuildContext context) {
    if(!logged)_login();
    return logged
        ? HomePage()
        : Scaffold(
            appBar: AppBar(
              shadowColor: Color.fromARGB(0, 0, 0, 0),
              backgroundColor: Color.fromARGB(0, 0, 0, 0),
              actions: [
                FlatButton(
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(18.0),
                      side: BorderSide(color: Colors.amber)),
                  color: Color.fromARGB(0, 0, 0, 0),
                  textColor: Colors.amber,
                  padding: EdgeInsets.all(8.0),
                  onPressed: () {
                    widget.signin();
                  },
                  child: Text(
                    "SIGN UP".toUpperCase(),
                    style: TextStyle(
                      fontSize: 14.0,
                    ),
                  ),
                ),
              ],
            ),
            body: SingleChildScrollView(
              child: Container(
                padding: EdgeInsets.all(20),
                child: Center(
                  child: Column(
                    children: [
                      Form(
                        key: _formKey,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[
                            Text("Log In",
                                style: TextStyle(
                                    color: Colors.amber,
                                    fontSize: 32,
                                    fontWeight: FontWeight.bold)),
                            SizedBox(height: 20),
                            TextFormField(
                              decoration: new InputDecoration(
                                labelText: "Email",
                                fillColor: Colors.white,
                                border: new OutlineInputBorder(
                                  borderRadius: new BorderRadius.circular(20.0),
                                  borderSide: new BorderSide(),
                                ),
                              ),
                              validator: (value) {
                                if (value.isEmpty ||
                                    !RegExp(r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+")
                                        .hasMatch(value)) {
                                  return 'Please enter a valid mail';
                                }
                                email = value;
                                return null;
                              },
                            ),
                            SizedBox(height: 20),
                            TextFormField(
                              obscureText: true,
                              decoration: new InputDecoration(
                                labelText: "Password",
                                fillColor: Colors.white,
                                border: new OutlineInputBorder(
                                  borderRadius: new BorderRadius.circular(20.0),
                                  borderSide: new BorderSide(),
                                ),
                              ),
                              validator: (value) {
                                if (value.isEmpty) {
                                  return 'Please enter a valid password';
                                }
                                if (value.length < 8) {
                                  return 'Please enter a password at least 8 characters long';
                                }
                                password = value;
                                return null;
                              },
                            ),
                            SizedBox(height: 20),
                            Center(
                              child: FlatButton(
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(18.0),
                                ),
                                color: Colors.amber,
                                onPressed: () async {
                                  if (_formKey.currentState.validate()) {
                                    _prefs.then((pref) {
                                      print("salvo");
                                      setState(() {});
                                        pref.setString("mail", email);
                                        pref.setString(
                                            "pass",
                                            sha256
                                                .convert(utf8.encode(password))
                                                .toString());
                                    });
                                  }
                                },
                                child: Text('Log In',
                                    style: TextStyle(color: Colors.black)),
                              ),
                            ),
                          ],
                        ),
                      )
                    ],
                  ),
                ),
              ),
            ),
          );
  }
  _login(){

    _prefs.then((pref) async {
      String mail, pass;
      try {
        mail = pref.getString("mail");
        pass = pref.getString("pass");
        print(mail+"---"+pass);
        if(mail!=null && pass!=null){
          print("loggin...");
          var res = await DataProvider.logIn(mail, pass);
          var j = jsonDecode(res.body);
          print(res.body);
          if (j["ok"] == true) {
            setState(() {
              logged = true;
            });
            user=new User(int.parse(j["user_data"]["id"]),j["user_data"]["mail"],j["user_data"]["nickname"],int.parse(j["user_data"]["coins"]),
              int.parse(j["user_data"]["reputazione"]),j["user_data"]["descrizione"],j["user_data"]["nome"],j["user_data"]["cognome"],
              int.parse(j["user_data"]["topic_preferito"]),j["user_data"]["citta"]);
          }
        }
      } on Exception catch (_) {}
    });
  }
}
