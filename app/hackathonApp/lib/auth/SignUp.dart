import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:hackathonApp/model/Topic.dart';
import 'package:hackathonApp/util/DataProvider.dart';

class SignUp extends StatefulWidget {
  SignUp(this.login);
  @override
  _SignUpState createState() => _SignUpState();

  final Function login;
}

class _SignUpState extends State<SignUp> {
  final _formKey = GlobalKey<FormState>();
  String firstname = "", lastname = "", nickname="", email = "", password = "",city="";
    Topic topic = DataProvider.topics[1];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        shadowColor: Color.fromARGB(0, 0, 0, 0),
        backgroundColor: Color.fromARGB(0, 0, 0, 0),
        actions: [
          FlatButton(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(18.0),
              side: BorderSide(color: Colors.amber)),
            color: Color.fromARGB(0, 0, 0, 0),
            textColor: Colors.amber,
            padding: EdgeInsets.all(8.0),
            onPressed: () {
              widget.login();
            },
            child: Text(
              "LOG IN".toUpperCase(),
              style: TextStyle(
                fontSize: 14.0,
              ),
            ),
            
          ),
        ],
      ),
      body: SingleChildScrollView(
        child: Container(
          padding: EdgeInsets.all(20),
          child: Center(
            child: Column(
              children: [
                Text("Sign Up", style: TextStyle(color: Colors.amber, fontSize: 32, fontWeight: FontWeight.bold)),
                Form(
                  key: _formKey,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      SizedBox(height: 20),
                      TextFormField(
                        decoration: new InputDecoration(
                          labelText: "First name",
                          fillColor: Colors.white,
                          border: new OutlineInputBorder(
                            borderRadius: new BorderRadius.circular(20.0),
                            borderSide: new BorderSide(),
                          ),
                        ),
                        validator: (value) {
                          if (value.isEmpty) {
                            return 'Please enter some text';
                          }
                          firstname = value;
                          return null;
                        },
                      ),
                      SizedBox(height: 20),
                      TextFormField(
                        decoration: new InputDecoration(
                          labelText: "Last name",
                          fillColor: Colors.white,
                          border: new OutlineInputBorder(
                            borderRadius: new BorderRadius.circular(20.0),
                            borderSide: new BorderSide(),
                          ),
                        ),
                        validator: (value) {
                          if (value.isEmpty) {
                            return 'Please enter some text';
                          }
                          lastname = value;
                          return null;
                        },
                      ),
                      SizedBox(height: 20),
                      TextFormField(
                        decoration: new InputDecoration(
                          labelText: "NickName",
                          fillColor: Colors.white,
                          border: new OutlineInputBorder(
                            borderRadius: new BorderRadius.circular(20.0),
                            borderSide: new BorderSide(),
                          ),
                        ),
                        validator: (value) {
                          if (value.isEmpty) {
                            return 'Please enter some text';
                          }
                          nickname = value;
                          return null;
                        },
                      ),
                      SizedBox(height: 20),
                      TextFormField(
                        decoration: new InputDecoration(
                          labelText: "Email",
                          fillColor: Colors.white,
                          border: new OutlineInputBorder(
                            borderRadius: new BorderRadius.circular(20.0),
                            borderSide: new BorderSide(),
                          ),
                        ),
                        validator: (value) {
                          if (value.isEmpty ||
                              !RegExp(r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+")
                                  .hasMatch(value)) {
                            return 'Please enter a valid mail';
                          }
                          email = value;
                          return null;
                        },
                      ),
                      SizedBox(height: 20),
                      TextFormField(
                        obscureText: true,
                        decoration: new InputDecoration(
                          labelText: "Password",
                          fillColor: Colors.white,
                          border: new OutlineInputBorder(
                            borderRadius: new BorderRadius.circular(20.0),
                            borderSide: new BorderSide(),
                          ),
                        ),
                        validator: (value) {
                          if (value.isEmpty) {
                            return 'Please enter a valid password';
                          }
                          if (value.length < 8) {
                            return 'Please enter a password at least 8 characters long';
                          }
                          password = value;
                          return null;
                        },
                      ),
                      SizedBox(height: 20),
                      TextFormField(
                        decoration: new InputDecoration(
                          labelText: "Your city",
                          fillColor: Colors.white,
                          border: new OutlineInputBorder(
                            borderRadius: new BorderRadius.circular(20.0),
                            borderSide: new BorderSide(),
                          ),
                        ),
                        validator: (value) {
                          if (value.isEmpty) {
                            return 'Please enter a valid city';
                          }
                          city = value;
                          return null;
                        },
                      ),
                      SizedBox(height: 20),
                      Text("Which is your main target?",style:TextStyle(fontSize: 18)),
                      Center(
                            child: DropdownButton<Topic>(
                              value: topic,
                              icon: Icon(Icons.arrow_drop_down),
                              iconSize: 24,
                              elevation: 16,
                              style: TextStyle(color: Colors.deepPurple),
                              underline: Container(
                                height: 2,
                              ),
                              onChanged: (Topic newValue) {
                                setState(() {
                                  topic = newValue;
                                });
                              },
                              items: DataProvider.topics
                                  .map<DropdownMenuItem<Topic>>((Topic c) {
                                return DropdownMenuItem<Topic>(
                                  value: c,
                                  child: Text(c.nome,
                                      style: TextStyle(
                                          color: Colors.amber, fontSize: 18)),
                                );
                              }).toList(),
                            ),
                          ),
                      Center(
                        child: FlatButton(
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(18.0),
                          ),
                          color: Colors.amber,
                          onPressed: () async {
                            if (_formKey.currentState.validate()) {
                              print(firstname +
                                  " " +
                                  lastname +
                                  " " +
                                  email +
                                  " " +
                                  password);
                              var res = await DataProvider.signUp(
                                  firstname, lastname, email, password,nickname,city,topic.id);
                              var j=jsonDecode(res.body);
                              if(j["ok"]==true){
                                widget.login();
                              }
                            }
                          },
                          child: Text('Sign Up',
                              style: TextStyle(color: Colors.black)),
                        ),
                      ),
                    ],
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
