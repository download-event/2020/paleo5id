import 'package:flutter/material.dart';
import 'package:hackathonApp/chat/ChatModel.dart';
import 'package:hackathonApp/chat/ChatPage.dart';
import 'package:hackathonApp/chat/UserChat.dart';
import 'package:scoped_model/scoped_model.dart';



class AllChatsPage extends StatefulWidget {
  final UserChat userChat;

  const AllChatsPage(this.userChat);
  @override
  _AllChatsPageState createState() => _AllChatsPageState();
}

class _AllChatsPageState extends State<AllChatsPage> {
  @override
  void initState() {
    super.initState();
    if(widget.userChat!=null){
      bool add=true;
      ChatModel.friendList.forEach((element) {
        if(element.chatID==widget.userChat.chatID)
          add=false;
      });
      if(add)
        ChatModel.friendList.add(widget.userChat);

    }
    ScopedModel.of<ChatModel>(context, rebuildOnChange: false).init();
  }

  void friendClicked(UserChat friend) {
    Navigator.of(context).push(
      MaterialPageRoute(
        builder: (BuildContext context) {
          return ChatPage(friend);
        },
      ),
    );
  }

  Widget buildAllChatList() {
    return ScopedModelDescendant<ChatModel>(
      builder: (context, child, model) {
        return ListView.builder(
          itemCount: ChatModel.friendList.length,
          itemBuilder: (BuildContext context, int index) {
            UserChat friend = ChatModel.friendList[index];
            return ListTile(
              title: Text(friend.name),
              onTap: () => friendClicked(friend),
            );
          },
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return  Scaffold(
      appBar: AppBar(
        title: Text('All Chats'),
        backgroundColor: Colors.amber,
      ),
      body: buildAllChatList(),
    );
  }
}
