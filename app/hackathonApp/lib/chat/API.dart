import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:http/http.dart';

class API {
//replace with your endpoint
  static String urlAPI = "https://hackathon.mattiaeffendi.me/user.php?id=";

  static Future<dynamic> getRequest(String id) async {
    Response res = await http.get(urlAPI + id);
    if (res.statusCode == 200) {
      return await jsonDecode(res.body);
    }
  }
}
