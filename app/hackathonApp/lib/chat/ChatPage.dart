import 'package:flutter/material.dart';
import 'package:hackathonApp/chat/UserChat.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:chat_bubbles/chat_bubbles.dart';

import './Message.dart';
import './ChatModel.dart';

class ChatPage extends StatefulWidget {
  final UserChat friend;
  ChatPage(this.friend);
  @override
  _ChatPageState createState() => _ChatPageState();
}

class _ChatPageState extends State<ChatPage> {
  final TextEditingController textEditingController = TextEditingController();

  Widget buildSingleMessage(Message message) {
    int a = !(message.senderID == widget.friend.chatID) ? 1 : 0;
    return Container(
      child: Column(children: <Widget>[
        BubbleNormal(
          text: (message.text),
          isSender: !(message.senderID == widget.friend.chatID),
          color: Colors.amber[a * 300],
          tail: true,
        ),
      ]),
    );
  }

  Widget buildChatList() {
    return ScopedModelDescendant<ChatModel>(
      builder: (context, child, model) {
        List<Message> messages =
            model.getMessagesForChatID(widget.friend.chatID);

        return Container(
          height: MediaQuery.of(context).size.height * 0.75,
          child: ListView.builder(
            itemCount: messages.length,
            itemBuilder: (BuildContext context, int index) {
              return buildSingleMessage(messages[index]);
            },
          ),
        );
      },
    );
  }

  Widget buildChatArea() {
    return ScopedModelDescendant<ChatModel>(
      builder: (context, child, model) {
        return Container(
          child: Row(
            children: <Widget>[
              SizedBox(width: 20,),
              Container(
                width: MediaQuery.of(context).size.width * 0.7,
                child: TextField(
                  controller: textEditingController,
                ),
              ),
              SizedBox(width: 10.0),
              FloatingActionButton(
                onPressed: () {
                  model.sendMessage(
                      textEditingController.text, widget.friend.chatID);
                  textEditingController.text = '';
                },
                elevation: 0,
                child: Icon(Icons.send),
              ),
            ],
          ),
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.friend.name),
        backgroundColor: Colors.amber,
      ),
      body: ListView(
        children: <Widget>[
          buildChatList(),
          buildChatArea(),
        ],
      ),
    );
  }
}
