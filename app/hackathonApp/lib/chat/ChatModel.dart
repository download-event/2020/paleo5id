import 'package:hackathonApp/chat/UserChat.dart';
import 'package:hackathonApp/main.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:flutter_socket_io/flutter_socket_io.dart';
import 'package:flutter_socket_io/socket_io_manager.dart';
import 'dart:convert';

import './UserChat.dart';
import './Message.dart';

class ChatModel extends Model {
  

  UserChat currentUserChat;
  static List<UserChat> friendList = List<UserChat>();
  List<Message> messages = List<Message>();
  SocketIO socketIO;

  void init() {
    currentUserChat = UserChat(user.nickname,user.id.toString());
    friendList=friendList.where((userChat) => userChat.chatID != currentUserChat.chatID).toList();
    socketIO = SocketIOManager().createSocketIO(
        'http://chat.hackathon.mattiaeffendi.me:8080', '/',
        query: 'chatID=${currentUserChat.chatID}');
    socketIO.init();
    socketIO.subscribe('receive_message', (jsonData) {
      Map<String, dynamic> data = json.decode(jsonData);
      messages.add(Message(
          data['content'], data['senderChatID'], data['receiverChatID']));
      notifyListeners();
    });
    socketIO.connect();
  }

  void sendMessage(String text, String receiverChatID) {
    messages.add(Message(text, currentUserChat.chatID, receiverChatID));
    print(currentUserChat.chatID+" "+ receiverChatID);
    socketIO.sendMessage(
      'send_message',
      json.encode({
        'receiverChatID': receiverChatID,
        'senderChatID': currentUserChat.chatID,
        'content': text,
      }),
    );
    notifyListeners();
  }

  List<Message> getMessagesForChatID(String chatID) {
    return messages
        .where((msg) => msg.senderID == chatID || msg.receiverID == chatID)
        .toList();
  }
}
