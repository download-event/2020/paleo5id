import 'package:hackathonApp/model/User.dart';

class Task{
  int id, idtopic;
  String citta, titolo, contenuto, hashtags,utentiProposti;
  int dataInizio,dataFine;
  DateTime inizio, fine;
  int durata, idCreatore,stato,tipo;
  User creatore;

  Task(this.id,this.idtopic,this.dataInizio,this.dataFine,this.durata,this.idCreatore,
    this.citta,this.utentiProposti,this.stato,this.titolo,this.contenuto,this.tipo,this.hashtags, this.creatore);
}