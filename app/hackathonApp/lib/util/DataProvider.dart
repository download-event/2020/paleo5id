import 'dart:convert';
import 'package:hackathonApp/model/Task.dart';
import 'package:hackathonApp/model/Topic.dart';
import 'package:hackathonApp/model/User.dart';
import 'package:http/http.dart' as http;
import 'package:crypto/crypto.dart';

class DataProvider{
  
  static Future<http.Response> signUp(String firstname, String lastname, String email, String password, String nickname,String citta, int pT) {
    var bytes1 = utf8.encode(email+firstname);
    var digest1 = sha1.convert(bytes1);
    var pass = sha256.convert(utf8.encode(password));
    print("reg "+pass.toString());
    return http.get("https://hackathon.mattiaeffendi.me/register.php?nome=$firstname&cognome=$lastname&mail=$email&password=$pass&token=$digest1&nickname=$nickname&citta=$citta&topic_preferito=$pT");
    
  }

  static Future<http.Response> logIn(String email, String sha256password) {
    print("Login "+sha256password);
    Digest digest1 = sha1.convert(utf8.encode(email+sha256password));
    return http.get("https://hackathon.mattiaeffendi.me/login.php?mail=$email&password=$sha256password&token=$digest1");
  }

  static Future<List<Task>> getTasks(String topicId,String citta,String durataMax) async {
    String url = "https://hackathon.mattiaeffendi.me/task.php?action=search&";
    if(topicId!=null){
      url+="topic=$topicId";
    }
    if(citta!=null){
      if(topicId!=null)
        url+="&";
      url+="citta=$citta";
    }
    if(durataMax!=null){
      if(citta!=null || topicId!=null)
        url+="&";
      url+="durata=$durataMax";
    }
    http.Response res = await http.get(url);
    
    List<Task> tasks=new List<Task>();
    var jdata = json.decode(res.body);
    for(int i=0;i<jdata["count"] && i<20;i++){
      var j=jdata["results"][i];
      User creatore = await DataProvider.getUser(int.parse(j["utente_creatore"]).toString());

      tasks.add(
        new Task(int.parse(j["id"]), int.parse(j["ID_topic"]), int.parse(j["data_inizio"]), j["data_fine"]!=null?int.parse(j["data_fine"]):null, int.parse(j["durata"]), int.parse(j["utente_creatore"]), j["citta"], "", int.parse(j["stato"]), j["titolo"],j["descrizione"],int.parse(j["tipo_task"]),j["hashtag"],creatore)
        );
    }

    return tasks;
  }
  static List<Topic> topics;

  static void getTopics() async {
    http.Response r =  await http.get("https://hackathon.mattiaeffendi.me/topic.php");
    topics=new List<Topic>();
    var jData=json.decode(r.body);
    for(int i=0;i<jData.length;i++)
      topics.add(Topic(int.parse(jData[i]["id"]),jData[i]["colore"],jData[i]["nome"]));
  }

  static String getTopicName(int id){
    String s="null";
    topics.forEach((element) {
      if(element.id.toString()==id.toString()){
        s=element.nome;
      }
    });
    return s;
  }
  static Future<User> getUser(String id) async {
    http.Response r =  await http.get("https://hackathon.mattiaeffendi.me/user.php?id=$id");
    var jData=json.decode(r.body);
    return User(int.parse(jData["user_data"]["id"]),jData["user_data"]["mail"],jData["user_data"]["nickname"],int.parse(jData["user_data"]["coins"]),
      int.parse(jData["user_data"]["reputazione"]),jData["user_data"]["descrizione"],jData["user_data"]["nome"],jData["user_data"]["cognome"],int.parse(jData["user_data"]["topic_preferito"]),jData["user_data"]["citta"],);
  }

  static Future<int> addTask(int durata,int utenteCreatore, String citta, String titolo, String contenuto,int idTopic) async {
    String url="https://hackathon.mattiaeffendi.me/task.php?action=insert&id=$idTopic&durata=$durata&utente_creatore=$utenteCreatore&citta=$citta&titolo=$titolo&tipo=0&hashtag=0&descrizione=$contenuto";
    print(url);

    http.Response res = await http.get(url);
    return res.statusCode;
  }
  



}