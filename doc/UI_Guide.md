## UI Guide

After the initial splashscreen there is a login form, for new users there is a sign up button in the right-hand corner of the screen, by clicking it, a form for registration will appear. you will then be asked to insert for with name, surname, nickname, mail, password, city and your focus topic.

<img title="" src="screenshots/LoginForm.png" alt="a" width="231" data-align="center">

Once you have logged in you will be brought to the home screen with the task view, divided in "Near you" and "Your focus topic".

            <img title="" src="screenshots/LoadingHomePage.png" alt="a" width="260" data-align="inline">            <img title="" src="screenshots/Homepage2.png" alt="a" width="260" data-align="inline">

In the bottom side there is a bar in which the first button is the "Home button", the second one is the "Chat button", the third one for "Settings" and the last for "User profile".

In the center-bottom side of the screen the Bee button is for create a new task, once clicked a new form is shown in which you have to set a title, time duration, description, city and the topic.

<img title="" src="screenshots/Createtask.png" alt="w" width="254" data-align="center">

Clicking on a task the task info page wil appear, in this screen you can see the topic, time, city and task creator of the task. You can also contact the creator via the contact "task creator" button, which will bring you to the chat menu (WIP)

           <img title="" src="screenshots/TaskInfo.png" alt="a" width="263">         <img src="screenshots/Chat.png" title="" alt="a" width="263">
